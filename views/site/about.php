<?php

/* @var $this yii\web\View */

use app\models\Monster;
use yii\helpers\Html;
use app\models\Monstertest;

$this->title                   = 'About';
$this->params['breadcrumbs'][] = $this->title;
$selectedMonster = (!Yii::$app->user->isGuest) ? Yii::$app->user->identity->name : '';
$monsterMap = \yii\helpers\ArrayHelper::map(Monster::find()->all(), 'name', 'name');
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        This is the About page. You will find information about our site, but nor garlic. We avoid garlic, because it is
        dangerous.
    </p>

    <p>
		<?php

		// Invalid Data
		$data = [
			'name'   => 'Wolfman',
			'age'    => 'invalidstring',
			'gender' => 'male'
		];


		// Valid Data
		//        $data = [
		//	        'name' => 'Wolfman',
		//	        'age' => '32',
		//	        'gender' => 'm'
		//        ];

		$validateMonster1 = new Monstertest($data);
		$validateMonster1->save();

		// Example of deleting a record
		//        $deleteMonster = Monstertest::findOne(['name' => 'Wolfman'])->delete();

		//Example of Yii Active Record
		$foundMonster1 = Monstertest::findOne(1);
		$foundMonster2 = Monstertest::findAll(['gender' => 'm']);

		?>

    <hr>
    <p>
        Fount Monster 1 Name: <?= $foundMonster1->name ?><br/>
        Fount Monster 2 Count: <?= count($foundMonster2) ?><br/>
    </p>
    <hr>
    <p>
        Validate Monster 1 Errors: <pre><?= var_dump($validateMonster1->errors) ?></pre>
    </p>
    <p>
        <?= Html::radioList('monsterName', $selectedMonster, $monsterMap); ?>
    </p>
    </p>

    <code><?= __FILE__ ?></code>
</div>
