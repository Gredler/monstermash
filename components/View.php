<?php
/**
 * Created by PhpStorm.
 * User: Andi
 * Date: 23.04.2018
 * Time: 11:09
 */

namespace app\components;

class View extends \yii\web\View
{
	function render($view, $params = [], $context = null)
	{
		return str_ireplace('garlic', 'g#%@c', parent::render($view, $params, $context));
	}
}